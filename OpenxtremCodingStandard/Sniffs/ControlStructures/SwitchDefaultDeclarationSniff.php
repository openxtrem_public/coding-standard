<?php

namespace OpenxtremCodingStandard\Sniffs\ControlStructures;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;

class SwitchDefaultDeclarationSniff implements Sniff
{
    /** @var int The number of spaces code should be indented. */
    public $indent = 4;

    /**
     * @inheritDoc
     */
    public function register()
    {
        return [T_SWITCH];
    }

    /**
     * @inheritDoc
     */
    public function process(File $phpcsFile, $stackPtr)
    {
        $tokens = $phpcsFile->getTokens();

        // We can't process SWITCH statements unless we know where they start and end.
        if (
            (isset($tokens[$stackPtr]['scope_opener']) === false)
            || (isset($tokens[$stackPtr]['scope_closer']) === false)
        ) {
            return;
        }

        $switch   = $tokens[$stackPtr];
        $nextCase = $stackPtr;

        $foundDefault = false;

        while (($nextCase = $this->findNextCase($phpcsFile, ($nextCase + 1), $switch['scope_closer'])) !== false) {
            // Skip nested SWITCH statements; they are handled on their own.
            if ($tokens[$nextCase]['code'] === T_SWITCH) {
                $nextCase = $tokens[$nextCase]['scope_closer'];
                continue;
            }

            if ($tokens[$nextCase]['code'] === T_DEFAULT) {
                $foundDefault = true;
            }
        }

        if ($foundDefault === false) {
            $error = 'All SWITCH statements must contain a DEFAULT case';
            $phpcsFile->addError($error, $stackPtr, 'MissingDefault');
        }
    }

    /**
     * Find the next CASE or DEFAULT statement from a point in the file.
     *
     * Note that nested switches are ignored.
     *
     * @param File $phpcsFile The file being scanned.
     * @param int  $stackPtr  The position to start looking at.
     * @param int  $end       The position to stop looking at.
     *
     * @return int|false
     */
    private function findNextCase(File $phpcsFile, int $stackPtr, int $end)
    {
        $tokens = $phpcsFile->getTokens();

        while (($stackPtr = $phpcsFile->findNext([T_CASE, T_DEFAULT, T_SWITCH], $stackPtr, $end)) !== false) {
            // Skip nested SWITCH statements; they are handled on their own.
            if ($tokens[$stackPtr]['code'] === T_SWITCH) {
                $stackPtr = $tokens[$stackPtr]['scope_closer'];
                continue;
            }

            break;
        }

        return $stackPtr;
    }
}
